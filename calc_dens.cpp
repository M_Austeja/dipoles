#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstring>
#include <vector>
#include <stdlib.h>
#include <cmath>

using namespace std;

const char failas[]="acetone_transdens.cub";
ifstream f1(failas);

int Natoms, Npoints;
double x_org, y_org, z_org;

double vol_data[200][200][200];

struct coord_vec{
    int points;
    double x;
    double y;
    double z;
};

struct atoms{
    double mass;
    double chg;
    double x;
    double y;
    double z;
};


void read_data(int &Natoms, int &Npoints, double vol_data[][200][200], vector <coord_vec> & axis, vector <atoms> & atom_list){
    string line;
    coord_vec V;
    atoms A;

    if (f1.is_open()){
        for (int i=0;i<2;i++){
            getline(f1,line,'\n');
        }
    }

    cout<<line<<endl;

    f1>>Natoms>>x_org>>y_org>>z_org;
    cout<<Natoms<<" "<<x_org<<" "<<y_org<<" "<<z_org<<endl;

    for (int i=0;i<3;i++){
        f1>>V.points>>V.x>>V.y>>V.z;
        axis.push_back(V);
        f1.ignore();
    }

    for (int i=0;i<3;i++){
        cout<<axis[i].points<<" "<<axis[i].x<<" "<<axis[i].y<<" "<<axis[i].z<<endl;
    }

    for (int i=0;i<Natoms;i++){
        f1>>A.mass>>A.chg>>A.x>>A.y>>A.z;
        atom_list.push_back(A);
        f1.ignore();
    }

    for (int i=0;i<3;i++){
        cout<<atom_list[i].mass<<" "<<atom_list[i].chg<<" "<<atom_list[i].x<<" "<<atom_list[i].y<<" "<<atom_list[i].z<<endl;
    }
cout<<axis[0].points<<" "<<axis[1].points<<" "<<axis[2].points<<endl;

    int q=0;
    for (int i=1;i<=axis[0].points;i++){
        for (int j=1;j<=axis[1].points;j++){
            for (int k=1;k<=axis[2].points;k++){
                f1>>vol_data[i][j][k];
                //cout<<vol_data[i][j][k]<<" ";
                q++;
                if (q==6){
                    f1.ignore();
                    q=0;
                }
            }
            //f1.ignore();
            q=0;
        }
    }

//    for (int i=1;i<=1;i++){
//        for (int j=1;j<=1;j++){
//            for (int k=1;k<=axis[2].points;k++){
//                cout<<vol_data[i][j][k]<<" ";
//                q++;
//                if (q==6){
//                    cout<<endl;
//                    q=0;
//                }
//            }
//            q=0;
//            cout<<endl;
//        }
//    }
}

void calc_dipole(int Natoms, double vol_data[][200][200], double dip_vec[3], vector <coord_vec> axis){

    double vx=0,vy=0,vz=0;
    double vol=axis[0].x*axis[1].y*axis[2].z;
    cout<<axis[0].points<<" "<<axis[1].points<<" "<<axis[2].points<<endl;
    cout<<vol<<endl;
    //int i=5;
    //vx=(i-1) * axis[0].x;
    //cout<<vx<<endl;


    for (int i=1;i<=axis[0].points;i++){
        for (int j=1;j<=axis[1].points;j++){
            for (int k=1;k<=axis[2].points;k++){
                 vx=(i-1) * axis[0].x + (j-1) * axis[1].x+(k-1)*axis[2].x;
                 vy=(i-1) *axis[0].y+(j-1)*axis[1].y+(k-1)*axis[2].y;
                 vz=(i-1) *axis[0].z+(j-1)*axis[1].z+(k-1)*axis[2].z;
                 dip_vec[0]+=vx*vol_data[i][j][k]*vol;
                 dip_vec[1]+=vy*vol_data[i][j][k]*vol;
                 dip_vec[2]+=vz*vol_data[i][j][k]*vol;
            }
        }
    }

    cout<<dip_vec[0]<<" "<<dip_vec[1]<<" "<<dip_vec[2]<<endl;
}

void print(double dip_vec[3]){

    string failas_new=failas;

    if (!failas_new.empty()) {
        failas_new.resize(failas_new.size() - 4);
    }

    failas_new+="_density.txt";
    ofstream o1(failas_new);

    o1<<setw(12)<<"mu_x"<<setw(12)<<"mu_y"<<setw(12)<<"mu_z"<<endl;

    for (int i=0;i<3;i++){
        o1<<setw(12)<<dip_vec[i];
    }
    o1<<endl;

}

int main(){
    vector <coord_vec> axis;
    vector <atoms> atom_list;
    double dip_vec[3]={0,0,0};

    read_data(Natoms,Npoints,vol_data,axis,atom_list);
    calc_dipole(Natoms,vol_data,dip_vec,axis);
    print(dip_vec);

    return 0;

}
